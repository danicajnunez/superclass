package com.mycompany.superclass;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author nunezd
 */
public class Subclass extends Superclass{

    //overrides printMethod in Superclass
    @Override
    public void printMethod(){
    super.printMethod();
    System.out.println("Printed in Subclass");
    }
    
    public static void main(String[] args) {
        Subclass s = new Subclass();
        s.printMethod();
    }
    
}
